Meteor.publish("groups", function(){
  return Groups.find();
});

Meteor.publish("workspaces", function(){
  return Workspaces.find();
});

Meteor.publish("users", function(){
  return Meteor.users.find();
});

Meteor.publish("commands", function(args){
  return Commands.find({workspace_id: args.workspace_id});
});
