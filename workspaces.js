Workspaces = new Meteor.Collection("workspaces");

Meteor.methods({
  create_workspace: function(user_id){
    var workspaces = Workspaces.find({user_id:user_id}).fetch();
    if(workspaces.length == 0){
      workspaces = Workspaces.insert({user_id : user_id});
    }else{
      workspace = workspaces[0];
    }
    return workspace.user_id;
  }
});