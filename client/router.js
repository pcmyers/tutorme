var TutorMe = Backbone.Router.extend({
  routes: {
    "workspace/:user_id": "workspace",
    '*path':  'defaultRoute'
  },
  workspace: function (user_id) {
    Meteor.call("create_workspace", user_id, function(error, result){
      Session.set("workspace_id", result);
      $("#workspace").show();
      $("#home").hide();
    });
  },
  defaultRoute: function(){
    $("#workspace").hide();
    $("#home").show();
  }
});

Router = new TutorMe();