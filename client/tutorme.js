Template.tutorme.users = function () {
  return Meteor.users.find();
};

Handlebars.registerHelper('user_string', function(context) {
    return (context[0].address);
});

Template.user.events({
  'click': function () {
    Router.navigate("workspace/"+this._id, true);
  }
});

