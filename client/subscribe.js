Meteor.subscribe("groups");
Meteor.subscribe("workspaces");
Meteor.subscribe("users");
Session.set("workspace_id",1);
Meteor.autosubscribe(function(){
  if(canvas){
    canvas.width = canvas.width;
  }
  Meteor.subscribe("commands", {workspace_id: Session.get("workspace_id")});
});